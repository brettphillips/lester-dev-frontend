import Vue from 'vue'
import Heatmap from './Heatmap.vue'

new Vue({
  el: '#app',
  render: h => h(Heatmap)
});
